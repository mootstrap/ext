Every variant has a libplatform.  For the embedded variants, this is usually
the standard peripheral libraries of the chip vendor.  For variants which have
no such library (like host_posix) we create a stand in here.
